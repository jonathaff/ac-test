package com.avenue.code.actest.model;

public class SimpleProduct {

  private Long id;

  private String name;

  private String description;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * Object based on Product object, used to create/return
   * objects without relationships.
   * @param id - Product Identity Number 
   * @param name - Name of Product
   * @param description - Description of Production
   */
  public SimpleProduct(Long id, String name, String description) {
    this.id = id;
    this.name = name;
    this.description = description;
  }
}
