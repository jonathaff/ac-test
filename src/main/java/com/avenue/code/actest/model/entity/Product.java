package com.avenue.code.actest.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@XmlRootElement
public class Product {
  
  public Product() {
  }
  
  /**
   * Entity / Database Table Product.
   * @param name - Name of Product
   * @param description - Description of Product
   * @param parentProduct - Parent Product (can be null)
   */
  public Product(String name, String description, Product parentProduct) {
    this.name = name;
    this.description = description;
    this.parentProduct = parentProduct;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  private String name;

  private String description;

  @ManyToOne
  private Product parentProduct;

  @OneToMany(fetch = FetchType.EAGER, mappedBy = "parentProduct")
  private List<Product> childrenList;

  @OneToMany(fetch = FetchType.EAGER, mappedBy = "product")
  @OnDelete(action = OnDeleteAction.CASCADE)
  private List<Image> imageList;
  
  public void setParentProduct(Product parentProduct) {
    this.parentProduct = parentProduct;
  }

  @XmlElement
  public Long getId() {
    return id;
  }

  @XmlElement
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @XmlElement
  public String getDescription() {
    return description;
  }
  
  public void setDescription(String description) {
    this.description = description;
  }

  @XmlElement
  @JsonIgnoreProperties("parentProduct")
  public List<Product> getChildrenList() {
    return childrenList;
  }

  public void setChildrenList(List<Product> childrenList) {
    this.childrenList = childrenList;
  }

  @XmlElement
  @JsonIgnoreProperties({ "parentProduct","product", "imageList" }) 
  public List<Image> getImageList() {
    return imageList;
  }

  public void setImageList(List<Image> imageList) {
    this.imageList = imageList;
  }

  @XmlElement
  public Product getParentProduct() {
    return parentProduct;
  }
 
}
