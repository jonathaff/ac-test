package com.avenue.code.actest.config;

import com.avenue.code.actest.service.rest.ImageResource;
import com.avenue.code.actest.service.rest.ProductResource;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
public class JerseyConfig extends ResourceConfig {
  
  /**
   * Class configuration to register the application resources.
   */
  public JerseyConfig() {
    register(ProductResource.class);
    register(ImageResource.class);
    register(MultiPartFeature.class);
  }

}