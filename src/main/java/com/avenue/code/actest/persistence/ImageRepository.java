package com.avenue.code.actest.persistence;

import com.avenue.code.actest.model.entity.Image;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface ImageRepository extends CrudRepository<Image, Long> {
  
  List<Image> findById(Long id);
  
}
