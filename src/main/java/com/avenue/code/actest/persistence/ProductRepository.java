package com.avenue.code.actest.persistence;

import com.avenue.code.actest.model.entity.Product;

import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Long> {
  
}
