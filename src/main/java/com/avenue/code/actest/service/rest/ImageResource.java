package com.avenue.code.actest.service.rest;

import com.avenue.code.actest.model.entity.Image;
import com.avenue.code.actest.persistence.ImageRepository;
import com.avenue.code.actest.persistence.ProductRepository;

import java.io.IOException;
import java.io.InputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
@Path("/image")
public class ImageResource {

  @Autowired
  ImageRepository imageRepository;
  
  @Autowired
  ProductRepository productRepository;

  /**
   * Subresource expect a HTTP GET method.
   * Get all attributes and relationships of Image
   * @param id - Image Identity Number
   * @return - the Image object in JSON format
   */
  @Path("{id}")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Image getSpecificImage(@PathParam("id") Long id) {
    return imageRepository.findOne(id);
  }

  /**
   * Subresource expect a HTTP PUT method, a form submission
   * uploading an image file.
   * @param id - Image Identity Number to Update
   * @param description - Description of Product to Persist
   * @param idProduct - Product Identity Number Associated
   * @param file - The file of Image
   * @param fileDisposition - The File Dispoistion
   * @return Image Object Persisted
   * @throws IOException - If some error occurs converting the InputStream
   */
  @Path("{id}")
  @PUT
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  @Produces(MediaType.APPLICATION_JSON)
  public Image updateImage(@PathParam("id") String id,
      @FormDataParam("description") String description,
      @FormDataParam("idProduct") String idProduct,
      @FormDataParam("file") InputStream file,
      @FormDataParam("file") FormDataContentDisposition fileDisposition) throws IOException {
    Image image = imageRepository.findOne(Long.valueOf(id));
    
    byte[] content = null;
    
    if (file != null) {
      content = IOUtils.toByteArray(file);
    }
    
    image.setDescription(description);
    image.setProduct(productRepository.findOne(Long.valueOf(idProduct)));
    image.setContent(content);
    return imageRepository.save(image);
  }

  /**
   * Subresource expect a HTTP DELETE method
   * Delete a Image from Database
   * @param id - Product Identity Number
   * @return HTTP Response Success
   */
  @Path("{id}")
  @DELETE
  @Produces(MediaType.APPLICATION_JSON)
  public Response deleteImage(@PathParam("id") Long id) {
    if (imageRepository.exists(id)) {
      imageRepository.delete(id);
    }
    return Response.ok("", MediaType.APPLICATION_JSON).build();
  }
  
  /**
   * Subresource expect a POST type HTTP Request, a form submission 
   * uploading an image file.
   * @param description - Description of Product to Persist
   * @param idProduct - Product Identity Number Associated
   * @param file - The file of Image
   * @param fileDisposition - The File Dispoistion
   * @return Image Object Persisted
   * @throws IOException - If some error occurs converting the InputStream
   */
  @POST
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  public Image saveImage(@FormDataParam("description") String description,
      @FormDataParam("idProduct") String idProduct,
      @FormDataParam("file") InputStream file,
      @FormDataParam("file") FormDataContentDisposition fileDisposition) throws IOException {
    byte[] content = null;
      
    if (file != null) {
      content = IOUtils.toByteArray(file);
    }
      
    Image image = new Image(description, content);
    image.setProduct(productRepository.findOne(Long.valueOf(idProduct)));
    return imageRepository.save(image);
  }

}
