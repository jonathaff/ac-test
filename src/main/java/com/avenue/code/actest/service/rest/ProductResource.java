package com.avenue.code.actest.service.rest;

import com.avenue.code.actest.model.SimpleProduct;
import com.avenue.code.actest.model.entity.Image;
import com.avenue.code.actest.model.entity.Product;
import com.avenue.code.actest.persistence.ProductRepository;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Path("/product")
public class ProductResource {

  @Autowired
  ProductRepository productRepository;

  /**
   * Subresource expect a HTTP POST method. 
   * @param product - Product Object to persist a new record
   * @return - the new persisted Product object in JSON format
   */
  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public Product saveProduct(Product product) {
    return productRepository.save(product);
  }

  /**
   * Subresource expect a HTTP GET method.
   * Get all attributes and relationships of product
   * @param id - Product Identity Number
   * @return - the Product object in JSON format
   */
  @Path("{id}")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Product getSpecificProduct(@PathParam("id") Long id) {
    return productRepository.findOne(id);
  }

  /**
   * Subresource expect a HTTP PUT method.
   * Update a Product Information.
   * @param product - Product Object to persist the existing record
   * @return - the updated persisted Product object in JSON format
   */
  @Path("{id}")
  @PUT
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public Product updateProduct(Product product) {
    return productRepository.save(product);
  }

  /**
   * Subresource expect a HTTP DELETE method.
   * Delete a Product from Database
   * @param id - Product Identity Number to Delete 
   * @return HTTP Response
   */
  @Path("{id}")
  @DELETE
  @Produces(MediaType.APPLICATION_JSON)
  public Response deleteProduct(@PathParam("id") Long id) {
    if (productRepository.exists(id)) {
      productRepository.delete(id);
    }
    return Response.ok("", MediaType.APPLICATION_JSON).build();
  }

  /**
   * Get all products excluding relationships (child products, images).
   * @return the list of products
   */
  @Path("complete/nochilds")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Iterable<SimpleProduct> getProductsNoAssociation() {
    List<SimpleProduct> newList = new ArrayList<SimpleProduct>();
    productRepository.findAll().forEach(x -> {
      newList.add(new SimpleProduct(x.getId(), x.getName(), x.getDescription()));
    });
    return newList;
  }
  
  /**
   * Get all products including specified relationships (child product and/or
   * images).
   * @return the list of products
   */
  @Path("complete")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Iterable<Product> getAllProducts() {
    return productRepository.findAll();
  }

  @Path("{id}/nochilds")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public SimpleProduct getProductNoAssociation(@PathParam("id") Long id) {
    Product p = productRepository.findOne(id);
    return p != null ? new SimpleProduct(p.getId(), p.getName(), p.getDescription()) : null;
  }

  /**
   * Returns a specific product details including relationships.
   * @param id - Product Identity Number Associated
   * @return the list of childs
   */
  @Path("{id}/childs")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Iterable<Product> getChildsProduct(@PathParam("id") Long id) {
    Product p = productRepository.findOne(id);
    if (p != null) {
      return p.getChildrenList();
    }
    return null;
  }

  /**
   * Get set of images for specific product.
   * @param id - Product Identity Number Associated
   * @return the list of images associated
   */
  @Path("{id}/images")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Iterable<Image> getSpecificImagesProduct(@PathParam("id") Long id) {
    Product p = productRepository.findOne(id);
    if (p != null) {
      return p.getImageList();
    }
    return null;
  }

}
