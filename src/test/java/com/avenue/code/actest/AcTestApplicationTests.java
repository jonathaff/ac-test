package com.avenue.code.actest;

import static org.junit.Assert.assertEquals;

import com.avenue.code.actest.model.SimpleProduct;
import com.avenue.code.actest.model.entity.Image;
import com.avenue.code.actest.model.entity.Product;
import com.avenue.code.actest.service.rest.ImageResource;
import com.avenue.code.actest.service.rest.ProductResource;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AcTestApplicationTests {

  @Autowired
  private ProductResource productResource;

  @Autowired
  private ImageResource imageResource;

  private static final String FILE = "beach.jpg";
  private static final String QUEUE = "qMultipart";
  private static final String EXPECTED = FILE + ":" + QUEUE;
  
  
  @Before
  public void setUp() {
  }

  @After
  public void tearDown() {
  }

  @Test
  public void productCreate() {
    Product product = new Product("Inserting new child product", "Inserting new child product",
        productResource.getSpecificProduct(1L));
    Product productReturned = productResource.saveProduct(product);
    org.junit.Assert.assertEquals(productReturned.getName(), product.getName());
  }

  @Test
  public void productRead() {
    Product product = productResource.getSpecificProduct(7L);
    org.junit.Assert.assertEquals(product.getDescription(), "E-reader");
  }

  @Test
  public void productUpdate() {
    Product product = productResource.getSpecificProduct(2L);
    product.setDescription("new description product 5");
    Product productReturned = productResource.updateProduct(product);
    org.junit.Assert.assertEquals(product.getId(), productReturned.getId());
  }

  @Test
  public void productDelete() {
    productResource.deleteProduct(5L);
  }

  @Test
  public void getAllProducts() {
    ArrayList<Product> listProducts = (ArrayList<Product>) productResource.getAllProducts();
    org.junit.Assert.assertNotNull(listProducts);
  }

  @Test
  public void getAllProductsNoAssociation() {
    ArrayList<SimpleProduct> listProducts = (ArrayList<SimpleProduct>) 
          productResource.getProductsNoAssociation();
    org.junit.Assert.assertNotNull(listProducts);
  }

  @Test
  public void getSpecificProductNoAssociation() {
    SimpleProduct simpleProduct = productResource.getProductNoAssociation(1L);
    org.junit.Assert.assertNotNull(simpleProduct);
  }

  @Test
  public void getSpecificChildsProduct() {
    Iterable<Product> childsProducts = productResource.getChildsProduct(7L);
    org.junit.Assert.assertNotNull(childsProducts);
  }

  @Test
  public void getSpecificImagesProduct() {
    Iterable<Image> imagesList = productResource.getSpecificImagesProduct(1L);
    org.junit.Assert.assertNotNull(imagesList);
  }

  @Test
  public void imageCreate() throws IOException {
    Image image = new Image("Inserting new Image", null);
    image.setProduct(productResource.getSpecificProduct(2L));
    Image imageReturned = imageResource.saveImage(image.getDescription(), "2", null, null);
    org.junit.Assert.assertEquals("Inserting new Image", imageReturned.getDescription());
  }

  @Test
  public void imageRead() {
    Image image = imageResource.getSpecificImage(10L);
    org.junit.Assert.assertEquals(image.getDescription(), "Lev IMAGE");
  }

  @Test
  public void imageUpdate() throws IOException {
    Product product = new Product();
    Image image = new Image("Update Test Image", null);
    image.setProduct(productResource.getSpecificProduct(7L));
    Image imageReturned = imageResource.getSpecificImage(2L);
    imageReturned.setDescription(image.getDescription());
    imageReturned.setProduct(image.getProduct());
    imageResource.updateImage(imageReturned.getId().toString(),
                              image.getDescription(),
                              image.getProduct().getId().toString(),
                              null, null);
    org.junit.Assert.assertEquals(imageReturned.getDescription(), "Update Test Image");
    
  }

  @Test
  public void deleteImage() throws IOException {
    Image image = new Image("Delete Image", null);
    image.setProduct(productResource.getSpecificProduct(1L));
    Image imageReturned = imageResource.saveImage(image.getDescription(), "1", null, null);
    org.junit.Assert.assertEquals(imageReturned.getDescription(), "Delete Image");
    imageResource.deleteImage(imageReturned.getId());
  }
  
}
