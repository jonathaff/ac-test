** REST API JAX-RS **
----

Service implementation using the concept of RESTFul Web Services and API's. The application was developed using Spring Boot with JPA/Hibernate,
Jersey (RESTful Web Services JAX-RS framework), Tomcat web container and h2 in-memory database and JUnit Tests framework. Maven is used to build/start 
the application and run tests.

  
The API provides the following functions:

1) CRUD (Create/Read/Update/Delete) - Entity Product and Entity Image

2) Get all products excluding relationships (child products, images)

3) Get all products including specified relationships (child product and/or images)

4) Get a specific product information excluding relationships 

5) Get a specific product information including relationships 

6) Get set of child products for specific product 

7) Get set of images for specific product
	
** Contents **

The mapping of the URI path space is presented in the following table:

URI path                              | Resource class      | HTTP methods                                             | Notes
------------------------------------- | ------------------- | -------------------------------------------------------- | --------------------------------------------------------
*/product                             |  ProductResource    |  POST                                                    |  Crate a new product.
*/product/{productId}                 |  ProductResource    |  GET, PUT (used for update product information), DELETE  |  Returns a specific product details including relationships
*/product/{productId}/nochilds        |  ProductResource    |  GET                                                     |  Returns a specific product details excluding relationships
*/product/{productId}/childs          |  ProductResource    |  GET                                                     |  Returns the child products of a parent product
*/product/complete                    |  ProductResource    |  GET                                                     |  Returns all products and relationships.
*/product/complete/nochilds           |  ProductResource    |  GET                                                     |  Returns all products excluding relationships.
*/image                               |  ImageResource      |  POST                                                    |  Create a new image associated with a product.
*/image/{imageId}                     |  ImageResource      |  GET, PUT (used for update image information), DELETE    |  Returns a specific image details including relationships.

**Prerequisites**

JDK 1.8
Maven 3

**Running Tests**

Maven command: mvn test

**Starting Application**

Maven command: mvn spring-boot:run

**Sample Calls**

**Product**


CREATE: curl -X POST http://localhost:8080/product/ -H 'Cache-control: no-cache' -H 'content-type: application/json' -d '{"name": "Galaxy Note 8","description": "Galaxy Note 8", "parentProduct" : {"id":1,"name":"Smartphone","description":"Smartphone"} }'


READ: curl http://localhost:8080/product/1


UPDATE: curl -X PUT http://localhost:8080/product/6 -H 'Cache-Control: no-cache' -H 'Content-Type: application/json'  -d '{ "name" : "Updating Item 6", "description": "Description Update item 1", "parentProduct" : {"id":"1"} }'


DELETE: curl -X DELETE http://localhost:8080/product/2 -H 'Cache-Control: no-cache'


**Image**

CREATE: curl -X POST http://localhost:8080/image/ -H 'Cache-Control: no-cache' -H 'Content-Type: multipart/form-data' -F 'description=S8 GEAR 3' -F idProduct=4 -F 'file=@/home/users/images/file_image.jpg'


READ: curl http://localhost:8080/image/6


UPDATE: curl -X PUT http://localhost:8080/image/8 -H 'Cache-Control: no-cache' -H 'Content-Type: multipart/form-data' -F 'description=updating kindle image' -F id=8 -F idProduct=9 -F 'file=@/home/users/images/file_image.jpg'


DELETE: curl -X DELETE http://localhost:8080/image/5 -H 'Cache-Control: no-cache'


Get all products excluding relationships (child products, images)

curl http://localhost:8080/product/complete/nochilds

Get all products including specified relationships (child product and/or images)

curl http://localhost:8080/product/complete

Returns a specific product details excluding relationships

curl http://localhost:8080/product/1/nochilds

Returns a specific product details including relationships

curl http://localhost:8080/product/1/childs

Get set of child products for specific product

curl http://localhost:8080/product/7/childs

Get set of images for specific product

curl http://localhost:8080/product/6/images


**Notes:**

In the DELETE request of Product automatically the associated images are also excluded.

The POST and PUT request of Image object expect a form submission uploading an image file, before execute the curl command it's necessary set a valid image path in the parameter "file", /home/users/images/file_image.jpg.